package resolution.example6.zzeulki.practice91;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    CounterTask mtask;
    boolean mbreak = true;

    public void startWork(View v){
        if(mbreak){
            mbreak = false;
            mtask = new CounterTask();
            mtask.setOuputView((TextView) findViewById(R.id.text1));
            EditText e = (EditText) findViewById(R.id.editText2);
            mtask.setMax(Integer.parseInt(e.getText().toString()));
            mtask.execute(10);
        }else{
            mbreak = true;
        }
    }

    public void cancelWork(View v) {
        mtask.onCancelled();
        mtask.cancel(mbreak);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
