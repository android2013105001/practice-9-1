package resolution.example6.zzeulki.practice91;

import android.os.AsyncTask;
import android.widget.TextView;

/**
 * Created by Zzeulki on 16. 10. 31..
 */
public class CounterTask extends AsyncTask<Integer, Integer, Integer> {
    TextView Output;
    int max = 0;
    int count = 0;

    public void setMax (int m)
    {
        max = m;
    }

    public void setOuputView (TextView txt){
        Output = txt;
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        count = 0;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        Output.setText("진행상황:" + values[0]+"\n 누적 합 :" + values[1]);
        super.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(Integer integer) {
        Output.setText("결과:" + integer);
        super.onCancelled(integer);
    }

    @Override
    protected void onCancelled() {
        Output.setText("취소됨" + "\n결과 : " + count);
        super.onCancelled();
    }

    @Override
    protected Integer doInBackground(Integer... params){
        for ( int i = 0 ; i< params[0]; i ++){
            if(i <= max)
                count += i;
            publishProgress(i,count);
            try{
                Thread.sleep(1000);
            }catch (InterruptedException e){;}
        }

        return count;
    }
}
